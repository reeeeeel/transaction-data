<!doctype html>
<html lang="en">
<head>
    <?php include('ui/header/head.php'); ?>
    <title>Dashboard</title>
</head>
<style>
    ._shadow:hover {
        box-shadow: 5px 5px 5px grey;
    }
</style>
<body>
<header>
    <?php include('ui/navbar/nav.php'); ?>
</header>
<main>
    <section class="p-2 ">
        <div class="container-fluid p-1">
            <div class="row g-2">
                <div class="col-8">
                    <div class="p-2 border bg-dark rounded-4 " style="height: 710px;">
                        <div class="container-fluid p-1">
                            <div id="product_data" class="row g-3">
                                <!--product-->

                            </div>
                        </div>
                    </div>
                </div>

                <!--add to cart-->
                <div class="col-4 p-2 border bg-dark rounded-4" style="height: 710px;">
                    <div class="col-6 bg-light p-2 border w-auto mb-2 rounded-4" style="height: 67%;">

                            <div class="vstack gap-2 col-md-12 mx-auto" id="add_order">


                                <div class="border border-success p-1 rounded-4 w-auto shadow-sm border-opacity-25">
                                    <div class="container-fluid">
                                        <div class="d-flex justify-content-between mt-2">
                                            <div class="container-fluid p-0">
                                                <h5 class="d-block">apple</h5>
                                                <h6 class="d-block text-muted">10$</h6>
                                            </div>
                                            <h6 class="text-muted mt-3">20Quantity</h6>
                                        </div>
                                    </div>
                                </div>

                            </div>
                    </div>
                    <div class="col-4 bg-light p-3 border w-auto rounded-4 mb-2" style="height: 21%;">

                    </div>
                    <div class="col-2 bg-light p-2 border w-auto rounded-4 mb-2" style="height: 10%;">
                        <div class="btn btn-outline-dark btn-lg rounded-4 _shadow" style="width: 29%; height: 100%;">Cancel</div>
                        <div class="btn btn-dark btn-lg rounded-4 _shadow" style="width: 70%; height: 100%;" onclick="build_rowDash()">Order</div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>
<!--script-->
<script src="script/product.dash.js"></script>
<script src="script/build_row.dash.js"></script>
</body>
</html>
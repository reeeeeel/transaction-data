<div class="btn-group m-2">
    <button type="button" class="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
        Add product
    </button>

    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-start">
        <!-- Dropdown menu links -->
        <li>
            <a type="button" class="dropdown-item" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Current product
            </a>
        </li>
        <li><h6 class="dropdown-header">More option</h6></li>
        <li>
            <button class="dropdown-item disabled" type="button">Available soon...</button>
        </li>

    </ul>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Add product</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                <form action="api/model/stock/update.php" method="get">

                    <div class="form-floating mb-3">
                        <input type="text" class="form-control user-select-none" id="name" name="name" placeholder="name">
                        <label for="floatingInput">Name</label>
                    </div>

                    <div class="form-floating mb-3">
                        <input type="number" class="form-control" id="qty" name="qty" placeholder="quantity">
                        <label for="floatingInput">Quantity</label>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary rounded-3" data-bs-dismiss="modal">Close </button>
                        <button type="submit" class="btn btn-secondary rounded-3">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


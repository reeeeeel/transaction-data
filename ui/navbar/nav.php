<style>
    .dropdown-toggle::after {
        content: none;
    }
</style>
</style>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow-sm">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Wut up!</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.php">Order</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#">Menu</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Table
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="product.php">product</a></li>
                        <li><a class="dropdown-item" href="stock.php">Stock</a></li>
                        <li><h6 class="dropdown-header user-select-none">More option</h6></li>
                        <li><a class="dropdown-item" href="#">Cancelling</a></li>

                    </ul>
                </li>
            </ul>
            <div class="btn-group ">
                <button type="button" class="btn btn-secondary dropdown-toggle rounded-4" data-bs-toggle="dropdown" aria-expanded="false">
                    Profile
                </button>
                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-start">
                    <!-- Dropdown menu links -->
                    <li><button class="dropdown-item" type="button">@davit_</button></li>
                    <li><button class="dropdown-item" type="button">Edit profile</button></li>
                    <li><h6 class="dropdown-header user-select-none">Configuration</h6></li>
                    <li><button class="dropdown-item" type="button">Setting</button></li>
                    <li><button class="dropdown-item" type="button">Sign out</button></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
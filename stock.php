
<!doctype html>
<html lang="en">
<head>
    <?php require('ui/header/head.php'); ?>
    <title>Product</title>
</head>
<body>
<header>
    <?php require_once('ui/navbar/nav.php'); ?>
</header>
<section>
    <?php include 'ui/page/stock_component/input.form.php'; ?>
</section>
<main>
    <?php include_once 'ui/page/stock_component/stock.table.php';?>

</main>
<!--script-->
<script src="script/stock.table.js"></script>
<?php require_once('ui/script/bootstrap_5_2.src.php'); ?>
</body>
</html>

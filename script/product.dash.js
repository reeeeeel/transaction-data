fetch("http://localhost/atech/Transaction_data/api/model/stock/read.pro_stock.php")
    .then(res => res.json())
    .then((json) => {
        let li = '';
        json.forEach((pro) => {
            li += `<div class="col-3">
                                   <div class="card user-select-none _shadow bg-body-tertiary rounded-4" style="height: 225px">
                                       <div class="card-header">
                                           ${status_mssge(pro.quantity, 'stts')}
                                       </div>
                                       <div class="card-body">
                                           <h5 class="card-title">${pro.name}</h5>
                                           <h6 class="card-subtitle mb-5 text-muted">${pro.price} $</h6>
                                       </div>
                                       <div class="d-flex align-items-end justify-content-between p-2">
                                           <button id="test" class="btn btn-outline-secondary rounded-4">
                                                Add order
                                                <!--add product qty -->
                                                ${status_mssge(pro.quantity, 'stts_num')}
                                           </button>
                                       </div>   
                                   </div>
                               </div>`;
        })
        document.getElementById('product_data').innerHTML = li;
    })

const status_mssge = (val, status) => {
    let conv_val = parseInt(val);
    switch (status) {
        case 'stts':
            return (
                (conv_val > 0) ?
                    `<span class="badge text-bg-success rounded-pill">Available</span>` :
                    `<span class="badge text-bg-danger rounded-pill">Unavailable</span>`
            )

        case 'stts_num':
            return (
                (conv_val > 0) ?
                    `<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-success">${conv_val}</span>` :
                    `<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">${conv_val}</span>`
            )
    }
}

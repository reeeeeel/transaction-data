fetch("http://localhost/atech/Transaction_data/api/model/stock/read.pro_stock.php")
    .then(res => res.json())
    .then((json) => {
        let li = '';
        let number = 1;
        json.forEach((pro) => {
            li += `
            <tr>
                <td>${number}</td>
                <td>${pro.code}</td>
                <td>${pro.name}</td>
                <td>${pro.quantity}</td>
                <td>${pro.price}</td>
                <td>${pro.created}</td>
                <td>${check_null(pro.update)}</td>
                <td class="text-center">
                    <a class="btn btn-outline-danger rounded-3 btn-sm" href="api/model/stock/delete.php?id=${pro.id}"><i class="fa fa-trash"></i></a>
                    <!-- Button trigger modal -->
                    <a type="button" class="btn btn-outline-warning rounded-3 btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal${pro.id}">
                        <i class="fa-solid fa-plus"></i>
                    </a>
                     <!-- Modal -->
                        <div class="modal fade" id="exampleModal${pro.id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="exampleModalLabel${pro.id}">Edit product</h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                    
                                      <form action="api/model/stock/update.php" method="get">
                                            <div class="form-floating mb-3">
                                                <input type="text" class="form-control user-select-none" id="name" name="name" value="${pro.name}" placeholder="name">
                                                <label for="floatingInput">Name</label>
                                            </div>
                        
                                            <div class="form-floating mb-3">
                                                <input type="number" class="form-control" id="qty" name="qty" placeholder="quantity">
                                                <label for="floatingInput">Quantity</label>
                                            </div>
                        
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-outline-secondary rounded-3" data-bs-dismiss="modal">Close </button>
                                                <button type="submit" class="btn btn-secondary rounded-3">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                </td>
            </tr>`;
            number++;
        })
        document.getElementById('stock_data').innerHTML = li;
    })
const check_null = (value) => (value == null) ? "" : value;
fetch("http://localhost/atech/Transaction_data/api/model/product/read.php")
    .then(res => res.json())
    .then((json) => {
        let li = '';
        json.forEach((pro) => {
            li += `
            <tr>
                <td>${pro.code}</td>
                <td>${pro.name}</td>
                <td>${pro.price}$</td>
                <td >
                    <a class="btn btn-outline-danger rounded-3" href="api/model/product/delete.php?code=${pro.code}"><i class="fa fa-trash"></i></a>
                    <!-- Button trigger modal -->
                    <a type="button" class="btn btn-outline-warning" data-bs-toggle="modal" data-bs-target="#exampleModal${pro.id}">
                        <i class="fa fa-pen"></i>
                    </a>
                     <!-- Modal -->
                        <div class="modal fade" id="exampleModal${pro.id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="exampleModalLabel${pro.id}">Edit product</h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                    
                                        <form action="api/model/product/update.php" method="get">
                                                
                                            <div class="form-floating mb-3">
                                              <input type="hidden" class="form-control user-select-none" id="id" name="id" value="${pro.id}" placeholder="id">
                                              <label for="floatingInput">Id</label>
                                            </div>                                          
                                            <div class="form-floating mb-3">
                                              <input type="text" class="form-control user-select-none" value="${pro.code}" placeholder="code" disabled>
                                              <label for="floatingInput">Code</label>
                                            </div>
                                             
                                             <div class="form-floating mb-3">
                                              <input type="text" class="form-control user-select-none" id="name" value="${pro.name}" placeholder="name" disabled>
                                              <label for="floatingInput">Name</label>
                                            </div>
                                           
                                            <div class="form-floating mb-3">
                                              <input type="number" class="form-control" id="price" name="price" placeholder="price">
                                              <label for="floatingInput">price</label>
                                            </div>
                                      
                                             <div class="modal-footer"> 
                                                <button type="button" class="btn btn-outline-secondary rounded-pill" data-bs-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-secondary rounded-pill">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                </td>
            </tr>`;
        })
        document.getElementById('data').innerHTML = li;
    })
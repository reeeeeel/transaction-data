<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include 'product.model.php';
$pro = new product();

$code = $_GET['code'];

if($pro->delete('product', $code)){
    header('Location: ../../product.php');
    (new validate)->message_status('product', 'delete', true );
}else{
    (new validate)-> message_status('product', 'delete', false );
}
header('location: http://localhost/Atech/Transaction_data/product.php');
exit();
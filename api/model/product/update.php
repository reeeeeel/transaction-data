<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include 'product.model.php';
$pro = new product();

$price = htmlspecialchars($_GET['price']);
$id = htmlspecialchars($_GET['id']);

print_r("test".$id. $price);
$value = [
    'price' => $price
];

if ($pro->update('product', $value, $id)) {
    (new validate)->message_status('product', 'update', true);

} else {
    (new validate)->message_status('product', 'update', false);
}

header('location: http://localhost/Atech/Transaction_data/product.php');
exit();

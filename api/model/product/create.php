<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include 'product.model.php';
$pro = new product();

$name = $_GET['name'];
$price = $_GET['price'];
$code = "P0_".rand(1,99999);

$value =[
    'code' => $code,
    'name' => $name,
    'price' => $price
];

if($pro->create('product', $value)){
    (new validate)->message_status('product', 'create', true );

}else{
    (new validate)->message_status('product', 'create', false );
}
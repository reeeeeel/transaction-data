<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include 'order_detail.model.php';
$odd = new order_detail();

$get_qty = $_GET['qty'];
$get_price = $_GET['price'];
$get_stk_id = $_GET['stk_id'];

$val_product = [
    'quantity' => $get_qty,
    'price' => $get_price,
    'stk_id' => $get_stk_id
];

$list = [];
array_push($list, $val_product);

print_r($list);

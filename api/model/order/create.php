<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include 'order.model.php';
$od = new order();

$curr_date = date("Y-m-d");
$gen_code = "OD_" . rand(1, 99999);
//$total = $_GET['total'];

$val_create = [
    'code' => $gen_code,
    'created' => $curr_date
];

//$val_total = [
//  'total_amount' => $total
//];

if($od->create('`order`', $val_create)){
    (new validate())->message_status('order', 'create', true);
}else{
    (new validate())->message_status('order', 'create', false);
}
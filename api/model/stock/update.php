<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include 'stock.model.php';
$st = new stock();

$get_name = $_GET['name'];
$get_qty = $_GET['qty'];
$curr_date = date("Y-m-d");

//get id from product with name
$sql_get_id = "SELECT id FROM product WHERE name = '$get_name'";
$stmt = $st->conn->query($sql_get_id);
$val_id = $stmt->fetch_assoc();
$get_id = $val_id['id'];

print_r('product id' . $get_id);

// get stock by product id
$sql_get_stk = "SELECT id FROM stock WHERE pro_id = '$get_id'";
$stmt_stk = $st->conn->query($sql_get_stk);
$val_stk_id = $stmt_stk->fetch_assoc();
$get_stk_id = $val_stk_id['id'];

print_r('get stock id ' . $sql_get_stk);

// value for create stock
$val_create = [
    'pro_id' => $get_id,
    'created' => $curr_date,
    'quantity' => $get_qty
];

// value (add) to stock_detail
$val_stk_dt = [
    'stk_id' => $get_stk_id,
    'action' => 'add',
    'qty' => $get_qty,
    'created' => $curr_date
];

//validate
if ($st->check_id($get_id)) {

    //update
    $sql_get_qty = "SELECT quantity FROM stock WHERE pro_id = '$get_id'";
    $stmt = $st->conn->query($sql_get_qty);
    $qty = $stmt->fetch_assoc();
    $result_qty = $qty['quantity'] + $get_qty;

    // value for update stock
    $val_update = [
        'update' => $curr_date,
        'quantity' => $result_qty
    ];

    // update value if exist in stock
    $st->update('stock', $val_update, $get_id);

    // add value to stock_detail
    $st->create('stock_detail', $val_stk_dt);

    (new validate())->message_status('stock', 'update', true);


} else {

    if ($st->create('stock', $val_create)) {

        // get stock by product id
        $sql_get_stk = "SELECT id FROM stock WHERE pro_id = '$get_id'";
        $stmt_stk = $st->conn->query($sql_get_stk);
        $val_stk_id = $stmt_stk->fetch_assoc();
        $get_stk_id = $val_stk_id['id'];

        // create new action into stock_detail
        $val_stk_dt_create = [
            'stk_id' => $get_stk_id,
            'action' => 'create',
            'qty' => $get_qty,
            'created' => $curr_date
        ];

        $st->create('stock_detail', $val_stk_dt_create);

        (new validate())->message_status('stock', 'create', true);
    }
}
header('location: http://localhost/Atech/Transaction_data/stock.php');
exit();


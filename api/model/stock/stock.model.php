<?php
include '../../../.env/default_database.php';
include '../../../.env/validate.php';
class stock extends default_database
{
    public function update($table, $data = [], $id = null):bool{
        $args = [];
        foreach($data as $key => $value){
            $args [] = "`$key` = '$value'";
        }
        $sql = "UPDATE $table SET ".implode(',', $args)." WHERE pro_id LIKE '$id'";
        print_r($sql);
        return $this->conn->query($sql);
    }

    public function check_id($id):bool{
        $r =false;
        $sql = "SELECT pro_id FROM product inner join stock s on product.id = s.pro_id";
        $stmt = $this->conn->query($sql);

        $pro_id = array();
        while($row = $stmt->fetch_assoc())$pro_id[]= $row;

        foreach ($pro_id as $value){
            if($value['pro_id'] == $id){
                $r = true;
            }
        }
        return $r;
    }

    // get product in stock
    public function read_product_all_stock(){
        $sql = "SELECT product.*, quantity, created, `update` FROM product inner join stock s on product.id = s.pro_id";
       return $this->conn->query($sql);
    }

    public function delete($table, $id = null):bool{
        $sql = "DELETE FROM $table WHERE pro_id LIKE '$id'";
        return $this->conn->query($sql);
    }
}
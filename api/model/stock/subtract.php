<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include 'stock.model.php';
$st = new stock();

$get_name = $_GET['name'];
$get_qty = $_GET['qty'];
$curr_date = date("Y-m-d");

//get id from product with name
$sql_get_id = "SELECT id FROM product WHERE name = '$get_name'";
$stmt = $st->conn->query($sql_get_id);
$val_id = $stmt->fetch_assoc();
$get_id = $val_id['id'];
//print ($get_id);

//print (gettype((int)$get_qty));

if ($st->check_id($get_id)) {

    //get quantity from stock by product id
    $sql_get_qty = "SELECT quantity FROM stock WHERE pro_id = '$get_id'";
    $stmt = $st->conn->query($sql_get_qty);
    $qty = $stmt->fetch_assoc();

    print ("qty = ".$qty['quantity']);

    if($qty['quantity'] >= $get_qty){

        $result_qty = $qty['quantity'] - $get_qty;

        // value for subtract stock
        $val_subtract = [
            'update' => $curr_date,
            'quantity' => $result_qty
        ];

        $st->update('stock', $val_subtract, $get_id);
            (new validate())->message_status('stock', 'update', true);
    }else{
        (new validate())->message_status('stock', 'update', false);
    }

}else{
    (new validate())->message_status('stock', 'update', false);

}
//header('location: http://localhost/Atech/Transaction_data/stock.php');
//exit();


<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


include 'stock.model.php';
$st = new stock();

$id = $_GET['pro_id'];
$created = date("Y-m-d");
$quantity = $_GET['qty'];

$value = [
    'pro_id' => $id,
    'created' => $created,
    'quantity' => $quantity
];

// select product id from table product
$sql = "SELECT id FROM product order by id";
$stmt = $st->conn->query($sql);

// validate
$product_id = [];
 while($row = $stmt->fetch_assoc()) $product_id[] = $row;
 // get value input
 $id_['id'] = $value['pro_id'];

 $result = array_diff_assoc($id_, $product_id);

 if($result  == $id_ ){
     if($st->create('stock', $value)){
         (new validate())->message_status('stock', 'create', 'success');
     }else{
         (new validate())->message_status('stock', 'create', 'fail');
     }
 }


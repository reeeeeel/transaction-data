<?php

class validate
{
    public function message_status($table, string $maintain = null, $status = true): void
    {
        if ($status) {
            echo json_encode(array("message" => "$maintain $table success"));
        } else {
            echo json_encode(array("message" => "$maintain $table false"));
        }
    }
}

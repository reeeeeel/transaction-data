# Transaction data concept
[![PHP Version Require](http://poser.pugx.org/pugx/badge-poser/require/php)](https://packagist.org/packages/pugx/badge-poser)
- version 1.2\
  This is project that under develop and have any messy code can be
  poison to reader, so anyway you can read code by structure above to understand how it works .
---
## Table Contents
* [Code Structure](#code-structure)
* [Schema Database](#schema-database)
---
### Code structure

- /.env
  - /test
- /api
  - /model
    - /cancelling
    - /order
    - /order_detail
    - /product
    - /stock
- /database
- /script
- /test
- /ui
  - /bootstrap
  - /header
  - /navbar
  - /page
    - /dashboard
    - /stock_component
  - /script
  - /sidebar
---

### Schema Database

![img.png](img.png)
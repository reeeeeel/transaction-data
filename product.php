<!doctype html>
<html lang="en">
<head>
    <?php require('ui/header/head.php'); ?>
    <title>Product</title>
</head>
<body>
<header>
    <?php require_once('ui/navbar/nav.php'); ?>
</header>
<main>
    <?php include_once 'ui/page/product.table.php';?>

</main>
<!--script-->
<script src="script/app.product.js"></script>
<?php require_once('ui/script/bootstrap_5_2.src.php'); ?>
</body>
</html>
